<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('availability_pair', function($attribute, $value, $parameters, $validator) {
            if(preg_match('/^\d{1}_\d{1}$/', $value)) {
                $pairValues = explode('_', $value);
                $dayOfWeekIsValid = \App\DayOfWeek::where('id', $pairValues[0])->exists();
                $shiftValueIsValid = in_array($pairValues[1], array_values(\App\Shift::SHIFT_TYPES));
                if($dayOfWeekIsValid && $shiftValueIsValid) {
                    return true; 
                }
            }

            return false;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
