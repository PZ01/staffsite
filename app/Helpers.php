<?php

namespace App;

class JsonHelper
{
    const JSON_KEY_SUCCESS = 'success';

    public static function success()
    {
        return [ JsonHelper::JSON_KEY_SUCCESS => 1 ];
    }

    public static function failure()
    {
        return [ JsonHelper::JSON_KEY_SUCCESS  => 0 ];
    }
}

class StringHelper
{
    protected static function getEmailLocalPart(string $email)
    {
        return explode("@", $email)[0];
    }

    protected static function stripSpecialCharacters(string $str)
    {
        return str_replace(['!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^', '_', '`', '{', '|', '}', '~'], '', $str);
    }

    public static function emailSlug(string $email)
    {
        $slug = StringHelper::getEmailLocalPart(StringHelper::stripSpecialCharacters($email));
        $cnt = User::where('slug', $slug)->count();

        if ($slug == '') {
            $slug = \Faker\Factory::create()->ean8();
        }
        if ($cnt > 0) {
            return $slug . $cnt;
        }
        return $slug;
    }
}

class FlashHelper
{
    /*
     * These constants must match the ones declared in 'Alert.vue'
     */
    const INFORMATION = 'info';
    const SUCCESS = 'success';
    const ERROR = 'error';

    public static function FlashBanner($message, $level = FlashHelper::INFORMATION)
    {
        $header = 'Information';
        if ($level == 'success') {
            $header = 'Success!';
        } elseif ($level == 'error') {
            $header = 'Error!';
        }
        session()->flash('flash_message_header', $header);
        session()->flash('flash_message', $message);
        session()->flash('flash_message_level', $level);
    }
}

class CollectionHelper
{
    public static function collectionIsSetAndNonEmpty($collection)
    {
        if (isset($collection)) {
            if ($collection->count() > 0) {
                return true;
            }
        }
        return false;
    }
}

interface MapProcessor
{
    public function processAddress();
}

class MapZenHelper implements MapProcessor
{
    protected $mapData = null;

    public function __construct($mapData)
    {
        $this->mapData = $mapData;    
    }

    /**
     * For some reason, MapZen adds a hashtag
     * before it's house numbers. This function
     * simply removes it.
     */
    protected function formatMapZenNumber($number)
    {
        return substr($number, 1);
    }

    /**
     * This overriden function processes a MapZen data result
     * and returns an Address Model. Please visit the
     * following link for details on the structure returned
     * by MapZen :
     * https://mapzen.com/documentation/search/response/#list-of-features-returned
     */
    public function processAddress()
    {
        $address = new Address([
            'house_number' => $this->formatMapZenNumber($this->mapData->properties->housenumber),
            'street' => $this->mapData->properties->street,
            'locality' => $this->mapData->properties->locality,
            'region' => $this->mapData->properties->region,
            'display_name' => $this->formatMapZenNumber($this->mapData->properties->label),
            'latitude' => $this->mapData->geometry->coordinates[1],
            'longitude' => $this->mapData->geometry->coordinates[0],
            'country_iso_code' => $this->mapData->properties->country_a
        ]);

        return $address;
    }
}
