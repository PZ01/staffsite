<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['house_number', 'street', 'locality', 
                           'region', 'postcode', 'display_name', 
                           'latitude', 'longitude', 'country_iso_code'];

    public function updateFromAddress(Address $address)
    {
        $this->house_number = $address->house_number;
        $this->street = $address->street;
        $this->locality = $address->locality;
        $this->region = $address->region;
        $this->display_name = $address->display_name;
        $this->latitude = $address->latitude;
        $this->longitude = $address->longitude;
        $this->country_iso_code = $address->country_iso_code;
        $this->save();
    }
}
