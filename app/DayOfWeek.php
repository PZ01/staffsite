<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayOfWeek extends Model
{
    protected $table = 'days_of_week';
}
