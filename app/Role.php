<?php 

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    const CANDIDATE = 'candidate';
    const BUSINESS = 'business';

    public static function candidateRoleId()
    {
        return static::where('name', self::CANDIDATE)->firstOrFail();
    }

    public static function businessRoleId()
    {
        return static::where('name', self::BUSINESS)->firstOrFail();
    }
}
