<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessAmbiance extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'business_ambiances';
}
