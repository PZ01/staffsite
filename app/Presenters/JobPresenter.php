<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class JobPresenter extends Presenter
{
    public function formattedBeginDate()
    {
        if ($this->begin_date != null) {
            return $this->begin_date->toDateString();
        }
        return '';
    }

    public function formattedEndDate()
    {
        if ($this->end_date != null) {
            return $this->end_date->toDateString();
        }
        return '';
    }
}
