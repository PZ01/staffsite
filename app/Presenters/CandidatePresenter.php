<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class CandidatePresenter extends Presenter
{
    public function formattedAge()
    {
        if($this->birthday != NULL) {
            return $this->birthday->age;
        }
        return 'Not Available';
    }

    public function formattedBirthday()
    {
        if($this->birthday != NULL) {
            return $this->birthday->toDateString();
        }
        return '';
    }

    public function formattedDriversLicense()
    {
        if($this->has_drivers_license) {
            return 'Yes';
        }
        return 'No';
    }

    public function formattedGender()
    {
        if($this->gender !== '') {
            return $this->gender;
        }
        return 'Not Available';
    }

    public function formattedOwnsCar()
    {
        if($this->owns_car) {
            return 'Yes';
        }
        return 'No';
    }
}
