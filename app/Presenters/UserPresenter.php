<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class UserPresenter extends Presenter
{
    public function formattedName()
    {
        if($this->name !== '') {
            return $this->name;
        }
        return 'Not Available';
    }
}
