<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use App\CollectionHelper;

class Candidate extends User
{
    /**
     * This trait provides a protected presenter variable
     * that acts as an information presenter of this model.
     */
    use PresentableTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['birthday'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'owns_car' => 'boolean',
        'has_drivers_license' => 'boolean',
    ];

    /**
     * The presenter class path that is used by this model.
     *
     * @var array
     */
    protected $presenter = \App\Presenters\CandidatePresenter::class;

    const CANDIDATE_MAX_JOB_LIMIT = 3;

    public static function store(string $email)
    {
        $user = parent::store($email);
        $candidate = Candidate::create();
        $candidate->baseUser()->save($user);
    }

    /**
     * Getters
     */
    public function speaksGivenLanguage(string $languageIsoCode)
    {
        //TODO validate input isoCode
        foreach ($this->spokenLanguages as $spokenLanguage) {
            if ($spokenLanguage->iso_code == $languageIsoCode) {
                return true;
            }
        }

        return false;
    }

    public function rate()
    {
        $rate = 0;
        $maxRate = 8;

        if($this->name !== '') {
            $rate++;
        }
        if($this->birthday !== '') {
            $rate++;
        }
        if($this->input_address !== '') {
            $rate++;
        }
        if($this->spokenLanguages->count() >= 1) {
            $rate++;
        }
        if($this->availabilities->count() >= 1) {
            $rate++;
        }

        $rate += $this->jobs->count();

        return ($rate / $maxRate) * 100;
    }

    public function isMale()
    {
        if ($this->gender == 'M') {
            return true;
        }
        return false;
    }
   
    public function speaksLanguages()
    {
        return CollectionHelper::collectionIsSetAndNonEmpty($this->languages);
    }

    public function hasJobs()
    {
        return CollectionHelper::collectionIsSetAndNonEmpty($this->jobs);
    }

    public function hasAvailabilities()
    {
        return CollectionHelper::collectionIsSetAndNonEmpty($this->availabilities);
    }

    public function isAvailableOnDay(int $dayOfWeekId)
    {
        return $this->availabilities->where('day_of_week_id', $dayOfWeekId)->first();
    }

    public function isAvailableForShift(int $dayOfWeekId, int $shiftId)
    {
        $availabilityOnGivenDay = $this->isAvailableOnDay($dayOfWeekId);
        if ($availabilityOnGivenDay == null) {
            return false;
        } else {
            return $availabilityOnGivenDay->shiftIsSet($shiftId);
        }
    }

    public function updateShiftForDay(int $dayOfWeekId, array $shiftValues)
    {
        $availabilityOnGivenDay = $this->isAvailableOnDay($dayOfWeekId);
        if ($availabilityOnGivenDay == null) {
            $avail = new Availability();
            $avail->day_of_week_id = $dayOfWeekId;
            $shiftTotal = Shift::convertShiftArrayToSum($shiftValues);
            if ($shiftTotal == -1) {
                return false;
            }
            $avail->shifts = $shiftTotal;
            $this->availabilities()->save($avail);
            return true;
        } else {
            $availabilityOnGivenDay->setShiftTotal($shiftValues);
            return true;
        }
    }

    public function hasCurrentJobId()
    {
        return $this->current_job_id != null;
    }
    
    public function hasAddress()
    {
        return $this->address_id != null;
    }

    public function hasMaxAmountOfJobs()
    {
        if ($this->jobs()->count() >= Candidate::CANDIDATE_MAX_JOB_LIMIT) {
            return true;
        }
        return false;
    }

    public function saveAddress(Address $address)
    {
        if($this->hasAddress()) {
            $this->address->updateFromAddress($address);
        } else {
            $this->address()->save($address);
            $this->address_id = $address->id;
            $this->save();
        }
    }

    /**
     * Relationships
     */
    public function baseUser()
    {
        return $this->morphMany('App\User', 'userable');
    }

    public function spokenLanguages()
    {
        return $this->belongsToMany('App\Language', 'candidate_language', 'candidate_id', 'language_iso_code');
    }

    public function availabilities()
    {
        return $this->hasMany('App\Availability');
    }

    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    public function address()
    {
        return $this->hasOne('App\Address', 'id', 'address_id');
    }
}
