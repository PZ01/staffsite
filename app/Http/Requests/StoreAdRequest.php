<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreAdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|required|max:50',
            'description' => 'required|max:500',
            'jobTitleId' => 'required|max:500|exists:job_titles,id',
            'employmentTypeId' => 'required|max:500|exists:employment_types,id',
            'mapData' => 'required',
        ];
    }
}
