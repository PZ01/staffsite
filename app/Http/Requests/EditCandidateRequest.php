<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditCandidateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'candidate.name' => 'required|string|max:50',
           'candidate.birthday' => 'date_format:Y-m-d|before:now',
           'candidate.gender' => 'string|max:1|in:M,F',
           'candidates.ownCar' => 'string|max:2|in:on',
           'languages.*' => 'string|max:2|exists:languages,iso_code',
           'availabilities.*' => 'availability_pair',
           'jobs.*.id' => 'required|integer|exists:jobs,id', //TODO jobs added trought the form will be handled differently
           'jobs.*.jobTitle' => 'required|string|max:2|exists:job_titles,id',
           'jobs.*.employer' => 'required|string|max:50',
           'jobs.*.beginDate' => 'required|date_format:Y-m-d',
           'jobs.*.endDate' => 'required|date_format:Y-m-d|after:jobs.*.beginDate',
           'jobs.*.description' => 'string|max:300',
           'currentJobId' => 'integer|exists:jobs,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'availabilities.*' => 'The availabilities provided are incorrect. Contact support.',
            'jobs.*.employer.required' => 'The employer job field is required.',
            'jobs.*.employer.max' => 'The employer job field cannot be bigger than :max characters.',
            'jobs.*.description.max' => 'The employer description field cannot be bigger than :max characters.',
            //TODO, add the other ones...
        ];
    }
}
