<?php

namespace App\Http\Controllers;

use App\User;
use App\Ad;
use App\Http\Requests;
use App\MapZenHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreAdRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;

class AdController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $isCurrentUser = Auth::user()->accessByCurrentUser($user);
        return view('dashboard.partials.business.ad.index', compact('isCurrentUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $isCurrentUser = Auth::user()->accessByCurrentUser($user);
        return view('dashboard.partials.business.ad.create', compact('isCurrentUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdRequest $request, User $user)
    {
        if($request->isMethod('post')) {
            \App\Ad::store($request, $user->userable_id);
        }
        return redirect('profile/' . Auth::user()->slug . '/ads');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @param  Ad $ad
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Ad $ad)
    {
        return view('dashboard.partials.business.ad.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
