<?php

namespace App\Http\Controllers\Auth;

use App\JsonHelper;
use App\FlashHelper;
use App\LoginToken;
use App\AuthenticatesUser;
use App\RegistersUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $authenticatesUser;
    protected $registersUser;

    public function __construct(AuthenticatesUser $authenticatesUser, RegistersUser $registersUser)
    {
        $this->authenticatesUser = $authenticatesUser;
        $this->registersUser = $registersUser;
    }

    public function logout()
    {
        $this->authenticatesUser->logout();

        return redirect('/');
    }

    public function login()
    {
        // TODO Add try catch
        $this->authenticatesUser->invite();
        
        FlashHelper::FlashBanner("A token has been sent to your inbox, please use it to sign-in.");
        return JsonHelper::success();
    }

    public function register()
    {
        if($this->registersUser->register() == JsonHelper::success()) {
            FlashHelper::FlashBanner('Account Created. Please verify your account by visiting your e-mail inbox.', FlashHelper::SUCCESS);
            $this->authenticatesUser->invite();
        }
    }

    public function showRegistrationForm()
    {
        return view('pages.register');
    }

    public function authenticate(LoginToken $token)
    {
        $this->authenticatesUser->login($token);

        return redirect('/profile/' . $token->user->slug);
    }
}
