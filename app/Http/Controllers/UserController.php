<?php

namespace App\Http\Controllers;

use App\User;
use App\FlashHelper;
use App\MapZenHelper;
use App\AvailabilityHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\EditCandidateRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',
            [
                'only' =>
                [
                    'showProfile',
                    'showProfileEdit',
                ]
            ]
        );
    }

    public function showProfile(User $user)
    {
        $isCurrentUser = Auth::user()->accessByCurrentUser($user);
        return view('dashboard.home', compact('user', 'isCurrentUser'));
    }

    public function showProfileEdit(User $user)
    {
        $isCurrentUser = Auth::user()->accessByCurrentUser($user);
        if ($isCurrentUser) {
            $edit = true;
            return view('dashboard.home', compact('user', 'isCurrentUser', 'edit'));
        } else {
            return redirect('/profile/' . Auth::user()->slug);
        }
    }

    public function patch(EditCandidateRequest $request, User $user)
    {
        if ($user->isCandidate()) {
            return $this->patchCandidate($request, $user);
        }
    }

    protected function availabilityInputToArray($inputAvailabilities)
    {
        $arr = [];
        foreach ($inputAvailabilities as $availabilityPair) {
            $pair = explode('_', $availabilityPair);
            if (array_key_exists($pair[0], $arr)) {
                $arr[$pair[0]][] = $pair[1];
            } else {
                $arr[$pair[0]] = [$pair[1]];
            }
        }
        return $arr;
    }

    protected function checkForNewUnavailabilities(User $user, $availabilities)
    {
        // When a candidate submits his availabilities and decides to remove all of his shifts
        // for a given day that previously had availabilithes the submitted data will not 
        // contain the days where all shifts for a given day were removed. Therefore we scan
        // the candidate's old availabilities and remove them if they are absent in the
        // candidate-submitted data.
        foreach ($user->userable->availabilities as $userAvailability) {
            $userDayOfWeekId = $userAvailability->dayOfWeek->id;
            $found = false;

            foreach (array_keys($availabilities) as $dayOfWeekId) {
                if ($userDayOfWeekId == $dayOfWeekId) {
                    $found = true;
                    break;
                }
            }
            if ($found == false) {
                $user->userable->availabilities()->where('day_of_Week_id', $userDayOfWeekId)->delete();
            }
        }
    }

    protected function patchCandidate(EditCandidateRequest $request, User $user)
    {
        if ($request->isMethod('patch') && Auth::user()->accessByCurrentUser($user)) {
            \DB::beginTransaction();
            $user->name = $request->input('candidate.name');
            $user->userable->birthday = $request->input('candidate.birthday');
            $user->userable->gender = $request->input('candidate.gender');
            if ($request->has('candidate.hasDriversLicense')) {
                $user->userable->has_drivers_license = true; 
            } else {
                $user->userable->has_drivers_license = false;
            }
            if ($request->has('candidate.ownsCar')) {
                $user->userable->owns_car = true;
            } else {
                $user->userable->owns_car = false;
            }
            if ($request->has('languages')) {
                $user->userable->spokenLanguages()->sync(array_values($request->input('languages')));
            }
            if ($request->has('availabilities')) {
                $availabilities = $this->availabilityInputToArray($request->input('availabilities'));
                $this->checkForNewUnavailabilities($user, $availabilities);
                foreach ($availabilities as $dayOfWeekId => $shiftValues) {
                    $user->userable->updateShiftForDay($dayOfWeekId, $shiftValues);
                }
            }
            if ($request->has('jobs')) {
                foreach ($request->input('jobs') as $jobFields) {
                    $userJob = $user->userable->jobs()->where('id', $jobFields['id'])->first();
                    $userJob->job_title_id = $jobFields['jobTitle'];
                    $userJob->employer = $jobFields['employer'];
                    $userJob->begin_date = $jobFields['beginDate'];
                    $userJob->end_date = $jobFields['endDate'];
                    $userJob->description = $jobFields['description'];
                    $userJob->save();
                }
                if($request->input('currentJobId') == null) {
                    if($user->userable->current_job_id != null) {
                        $user->userable->current_job_id = null;
                    }
                } 
                else {
                    $user->userable->current_job_id = $request->input('currentJobId');
                }
            }
            if( ! empty($request->input('mapData'))) {
                $mapData = json_decode(urldecode($request->input('mapData')));
                if($mapData == null) {
                    //TODO, raise an error or something...
                } 
                else {
                    $mapZenHelper = new MapZenHelper($mapData);
                    $user->userable->saveAddress($mapZenHelper->processAddress());
                }
            }
            $user->save();
            $user->userable->save();

            \DB::commit();
        }
        return redirect('profile/' . Auth::user()->slug);
    }
}
