<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
||*/

Route::get('/', 'PagesController@home');
Route::get('auth/token/{token}', 'Auth\AuthController@authenticate');
Route::get('logout', 'Auth\AuthController@logout');
Route::post('register', 'Auth\AuthController@register');
Route::post('login', 'Auth\AuthController@login');

Route::get('/profile/{user}', 'UserController@showProfile');
Route::get('/profile/{user}/edit', 'UserController@showProfileEdit');
Route::patch('/profile/{user}/patch', 'UserController@patch');

Route::get('/profile/{user}/ads/', 'AdController@index');
Route::get('/profile/{user}/ads/{ad}', 'AdController@show');
Route::get('/profile/{user}/ads/create', 'AdController@create');
Route::post('/profile/{user}/ads/store', 'AdController@store');

