<?php

namespace App;

use Laracasts\Presenter\PresentableTrait;
use App\Http\Requests\StoreAdRequest;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    /**
     * This trait provides a protected presenter variable
     * that acts as an information presenter of this model.
     */
    use PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expiry_date', 'posted_date'];


    public static function store(StoreAdRequest $request, int $businessId)
    {
        $ad = new Ad();
        $ad->title = $request->title;
        $ad->description = $request->description;
        $ad->job_title_id = $request->jobTitleId;
        $ad->business_id = $businessId;
        $ad->employment_type_id = $request->employmentTypeId;
        $ad->posted_date = \Carbon\Carbon::now();
        $ad->expiry_date = $ad->posted_date->addWeeks(2);
        $mapZenHelper = new MapZenHelper(json_decode(urldecode($request->input('mapData'))));
        $address = $mapZenHelper->processAddress();
        $ad->address()->save($address);
        $ad->address_id = $address->id;

        $ad->save();
    }

    public function hasAddress()
    {
        return $this->address_id != null;
    }

    public static function publicColumnTitles()
    {//TODO, might want to move this elsewhere...
        return collect([
            'Title',
            'Job',
            'Employment',
            'Active',
        ]);
    }

    public static function filteredAds($businessId)
    {
        return Ad::join('job_titles', 'ads.job_title_id', '=', 'job_titles.id')
            ->where('ads.business_id', '=', $businessId)
            ->join('employment_types', 'ads.employment_type_id', '=', 'employment_types.id')
            ->select('ads.id', 'title as Title', 'job_titles.display_name as Job', 'employment_types.display_name as Employment', \DB::raw('ads.expiry_date > NOW() as Active'))
            ->get();
    }

    public function isActive() {
        return $this->expiry_date->gt(\Carbon\Carbon::now());
    }

    /**
     * Eloquent Relationships
     */
    public function schedules()
    {
        return $this->hasMany('App\Schedule');
    }

    public function address()
    {
        return $this->hasOne('App\Address', 'id', 'address_id');
    }
}
