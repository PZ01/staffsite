<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateLanguage extends Model
{
    /**
     * Eloquent automatically assumes that timestamp columns are present
     * on the table storing the Model data. To prevent this feature the
     * $timestamps variable turns off this feature.
     */
    public $timestamps = false;

    /**
     * Eloquent assumes the primary keys are integers and will automatically cast them to integers. 
     * For any primary key that is not an integer we override the $incrementing property to prevent
     * the automatic casting.
     */
    public $incrementing = false;

    protected $table = 'candidate_language';
    protected $fillable = ['candidate_id', 'language_iso_code'];
}
