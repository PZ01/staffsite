<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laracasts\Presenter\PresentableTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

//PZ::TODO It would be interesting to make this class abstract, don't know how well laravel would behave
class User extends Authenticatable
{
    /**
     * This will enable the relation with Role and add the following methods: 
     * roles(), hasRole($name), can($permission), and ability($roles, $permissions, $options) 
     * within the User model.
     */
    use EntrustUserTrait;

    /**
     * This trait provides a protected presenter variable
     * that acts as an information presenter of this model.
     */
    use PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token'
    ];

    /**
     * The presenter class path that is used by this model.
     *
     * @var array
     */
    protected $presenter = \App\Presenters\UserPresenter::class;

    public function subUser()
    {
        switch ($this->userable_type) {
            case Candidate::Class:
                return Candidate::find($this->id);
            case Candidate::Business:
                return Business::find($this->id);
        }
    }

    protected static function store(string $email)
    {
        $user = new User;
        $user->email = $email;
        $user->slug = StringHelper::emailSlug($email, User::count());

        return $user;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function isCandidate()
    {
        if ($this->userable_type == Candidate::class) {
            return true;
        }
        return false;
    }

    public function isBusiness()
    {
        if ($this->userable_type == Business::class) {
            return true;
        }
        return false;
    }

    public static function byEmail($email)
    {
        return static::where('email', $email)->firstOrFail();
    }

    public static function emailExists($email)
    {
        if (is_null(static::where('email', $email)->first())) {
            return false;
        }
        return true;
    }

    public function getEmailName()
    {
        return explode("@", $this->email)[0];
    }

    public function accessByCurrentUser(User $requestedUser)
    {
        if ($this->slug == $requestedUser->slug) {
            return true;
        }

        return false;
    }

    public function userable()
    {
        return $this->morphTo();
    }
}
