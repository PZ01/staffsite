<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * Eloquent assumes the primary keys are integers and will automatically cast them to integers. 
     * For any primary key that is not an integer we override the $incrementing property to prevent
     * the automatic casting.
     */
    public $incrementing = false;

    /**
     * Eloquent will also assume that each table has a primary key column named id. 
     * The $primaryKey property is used to to override this convention.
     */
    protected $primaryKey = 'iso_code';
}
