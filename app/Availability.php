<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected $table = 'availabilities';
    public $timestamps = false;

    public function dayOfWeek()
    {
        return $this->hasOne('App\DayOfWeek', 'id', 'day_of_week_id');
    }

    protected function availability() {
        return \DB::table($this->table)
            ->where([
                ['candidate_id', '=',  $this->candidate_id],
                ['day_of_week_id', '=', $this->day_of_week_id]
            ]);
    }

    public function setShift(...$shiftTypes)
    {
        $this->availability()->update(['shifts' => array_sum($shiftTypes)]);
    }

    public function setShiftTotal(array $shiftValues)
    {
        $this->availability()->update(['shifts' => Shift::convertShiftArrayToSum($shiftValues)]);
    }

    public function activeShifts()
    {
        return Shift::activeShiftsByName($this->shifts);
    }

    public function shiftIsSet($shiftType)
    {
        if ($this->shifts & $shiftType) {
            return true;
        }
        return false;
    }
}
