<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    public $timestamps = false;

    public function dayOfWeek()
    {
        return $this->hasOne('App\DayOfWeek', 'id', 'day_of_week_id');
    }

    protected function schedule() {
        return \DB::table($this->table)
            ->where([
                ['ad_id', '=',  $this->ad_id],
                ['day_of_week_id', '=', $this->day_of_week_id]
            ]);
    }

    public function setShift(...$shiftTypes)
    {
        $this->schedule()->update(['shifts' => array_sum($shiftTypes)]);
    }

    public function setShiftTotal(array $shiftValues)
    {
        $this->schedule()->update(['shifts' => Shift::convertShiftArrayToSum($shiftValues)]);
    }

    public function activeShifts()
    {
        return Shift::activeShiftsByName($this->shifts);
    }

    public function shiftIsSet($shiftType)
    {
        if ($this->shifts & $shiftType) {
            return true;
        }
        return false;
    }
}
