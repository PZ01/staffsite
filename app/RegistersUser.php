<?php

namespace App;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;

class RegistersUser
{
    use ValidatesRequests;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function register()
    {
        $this->validateRegistrationRequest();    

        if($this->request->isMethod('post')) {

            if($this->request->input('user_type') == 'candidate') {
                Candidate::store($this->request->email);
            }
            else if($this->request->input('user_type') == 'business') {
                Business::store($this->request->email);
            }

            return JsonHelper::success();
        }
        return JsonHelper::failure();
    }

    protected function validateRegistrationRequest()
    {
        $this->validate($this->request, [
            'email' => 'required|email|unique:users'
        ]);
    }
}
