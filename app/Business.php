<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends User
{
    public function baseUser()
    {
        return $this->morphMany('App\User', 'userable');
    }

    public static function store(string $email)
    {
        $user = parent::store($email);
        $business = Business::create();
        $business->baseUser()->save($user);
    }

    public function businessType()
    {
        return $this->hasOne('App\BusinessType', 'id', 'business_type_id');
    }

    public function businessAmbiance()
    {
        return $this->hasOne('App\BusinessAmbiance', 'id', 'business_ambiance_id');
    }
}
