<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift
{
    const SHIFT_TYPES = ['AM' => 1, 'PM' => 2, 'Night' => 4];

    public static function shiftCombinations()
    {
        return pow(2, count(array_slice(self::SHIFT_TYPES, 1)));
    }

    public static function activeShiftsByName($shifts)
    {
        $namedShifts = [];
        foreach (Shift::SHIFT_TYPES as $key => $val) {
            if ($shifts & $val) {
                array_push($namedShifts, $key);
            }
        }
        return $namedShifts;
    }

    public static function convertShiftArrayToSum(array $shifts)
    {
        $correctInputs = 0;

        $hasMoreShiftsThanPossible = count($shifts) > self::shiftCombinations();
        if($hasMoreShiftsThanPossible) {
            return -1; 
        }

        $hasDuplicates = count($shifts) !== count(array_unique($shifts)); 
        if($hasDuplicates) {
            return -1; 
        }

        foreach($shifts as $shift) {
            foreach(array_values(self::SHIFT_TYPES) as $correctShift) {
                if($shift == $correctShift) {
                    $correctInputs++;
                    break;
                }
            }
        }

        $hasInvalidShiftValues = $correctInputs !== count($shifts);
        if($hasInvalidShiftValues) {
            return -1;
        }

        $shiftSum = array_sum($shifts);

        $shiftSumOverMaximum = $shiftSum > array_sum(array_values(self::SHIFT_TYPES));
        if($shiftSumOverMaximum) {
            return -1;
        }

        return $shiftSum;
    }
}
