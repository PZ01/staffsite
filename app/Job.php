<?php

namespace App;

use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * This trait provides a protected presenter variable
     * that acts as an information presenter of this model.
     */
    use PresentableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'current' => 'boolean',
    ];

    /**
     * The presenter class path that is used by this model.
     *
     * @var array
     */
    protected $presenter = \App\Presenters\JobPresenter::class;

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['begin_date', 'end_date'];

    public function duration()
    {
        return $this->begin_date->diffForHumans($this->end_date, true);
    }

    public function jobTitle()
    {
        return $this->hasOne('App\JobTitle', 'id', 'job_title_id');
    }
}
