<?php

namespace App;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;

class AuthenticatesUser
{
    use ValidatesRequests;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function invite()
    {
        $loginToken = $this->validateTokenRequest($this->request)->createToken()->send();
    }

    public function login(LoginToken $token)
    {
        Auth::login($token->user);
        $token->delete();
    }

    public function logout()
    {
        Auth::logout();
    }

    protected function createToken()
    {
        $user = User::byEmail($this->request->email);
        return LoginToken::generateFor($user);
    }

    protected function validateTokenRequest()
    {
        $this->validate($this->request, [
            'email' => 'required|email|exists:users'
        ]);
       
        return $this;
    }
}
