var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass(['app.scss', 'jumbotron-narrow.scss', 'datepicker_standalone3.css']);
    mix.browserify('app.js')
       .browserify('edit_candidate.js')
       .browserify('create_ad.js');
    mix.version(['css/app.css', 'js/app.js', 'js/create_ad.js', 'js/edit_candidate.js']);

    // Copy the bootstrap fonts to be publicly served.
    mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/','public/build/fonts/bootstrap');
    // Copy the Leaflet map marker images to be publicly served
    mix.copy('node_modules/leaflet/dist/images/','public/images');
});
