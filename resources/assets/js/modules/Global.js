module.exports = function () {
$( document ).ready(function() {
    function setupTooltips()
    {
        $('[data-toggle="tooltip"]').tooltip();
    }

    setupTooltips();
});

}
