let baseUrl              = document.getElementsByName('base_url')[0].getAttribute('content');

window.$ = window.jQuery = require('jquery')
                           require("bootstrap-datepicker")
                           require('bootstrap-sass');
var Vue                  = require('vue');
var Global               = require('./modules/Global.js')();

import BootstrapModalAjaxHandler from './modules/BootstrapModalAjaxHandler.js';
import Alert                     from './components/Alert.vue';
import Map                       from './components/Map.vue';
import Grid                      from './components/Grid.vue';

$( document ).ready(function() {
    BootstrapModalAjaxHandler();

    new Vue({
        el: 'body',
        components: { Alert, Map, Grid }
    });
});
