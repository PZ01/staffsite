var Nod = require('nod-validate');
Nod.classes.successClass = 'has-success';
Nod.classes.errorClass = 'has-error';
Nod.classes.successMessageClass = 'help-block';
Nod.classes.errorMessageClass = 'help-block';

var nod = Nod();

nod.configure({
    submit: '.submit-btn',
    disableSubmit: true
});

nod.add([{
    selector: '#ad-title',
    validate: 'between-length:10:50',
    errorMessage: 'Ad title should be between 10 and 50 characters.'
}, {
    selector: '#ad-description',
    validate: 'presence',
    errorMessage: 'Description can\'t be empty'
}]);
