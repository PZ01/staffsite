$( document ).ready(function() {

    const CHECKED_FLAG = 'checked';

    function setupBirthdayPicker() {
        $('#editCandidateProfile .input-group.date').datepicker({
            format: "yyyy-mm-dd",
            endDate: "0d",
            startView: 2,
            clearBtn: true,
            autoclose: true
        });
    }

    function setupExperienceRangePickers() {
        $('#editCandidateProfile .input-experience-daterange input').each(function() {
            $(this).datepicker({
                format: "yyyy-mm-dd",
                endDate: "0d",
                clearBtn: true,
                autoclose: true
            });
        });
    }
   
    function setupLanguageSelectionList() {
        List = require('list.js');

        let languageList = new List('languageList', {
          valueNames: [ 'languageName', 'languageKnown', 'isoCode' ]
        });

        $('#showAllLanguages').change(function() {
            if (this.checked) {
                languageList.filter();
            } else {
                languageList.filter(function(item) {
                    if (item.values().languageKnown == CHECKED_FLAG) {
                        return true;
                    } else {
                        return false;
                    }
                });
            }
        });

        $('#languageList li input[type=checkbox]').change(function() {
                if (this.checked) {
                    languageList.get('isoCode', this.value)[0].values().languageKnown = CHECKED_FLAG;
                } else {
                    languageList.get('isoCode', this.value)[0].values().languageKnown = '';
                }
        });
    }
            
    function setupCurrentWorkPlaceLogic() {
        let currentlyWorkingButtons = $('.current-job-btns').children();
        let checkmarkSpan = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';

        function deselect(jqSelector) {
            jqSelector.children('span').remove();
            jqSelector.removeClass('current-job-selected btn-success');
            jqSelector.addClass('btn-default'); 
        }

        function select(jqSelector) {
            jqSelector.addClass('current-job-selected btn-success');
            jqSelector.removeClass('btn-default');
            jqSelector.append(checkmarkSpan);
        }

        function clearAllSelected() {
            $('.btn-toolbar .current-job-selected').each(function() {
                if($(this).hasClass('current-job-selected')) {
                    deselect($(this));
                }
            });
        }

        currentlyWorkingButtons.each(function() {
            // Add a checkmark on the selected experience upon loading
            // the form.
            if($(this).hasClass('current-job-selected')) {
                $(this).append(checkmarkSpan);
            }

            $(this).click(function() {
                let currentlySelected = false;
                if($(this).hasClass('current-job-selected')) {
                    currentlySelected = true;
                }

                clearAllSelected();

                if(currentlySelected) {
                    deselect($(this));
                    $('#current-job-hidden-input').val(null);
                } else {
                    select($(this));
                    // The button has a value equal to the experience id. This
                    // sets the global hidden input to this value.
                    $('#current-job-hidden-input').val($(this).attr('value'));
                }
            });
        });
    }

    setupBirthdayPicker();
    setupExperienceRangePickers();
    setupLanguageSelectionList();
    setupCurrentWorkPlaceLogic();
}); // End Document.Ready
