<div class="dropdown">
    <button type="button" 
            class="btn btn-default dropdown-toggle" 
            id="user-dropdown"
            data-toggle="dropdown" 
            aria-haspopup="true" 
            aria-expanded="true">
            <img class="img-thumbnail" src="http://dummyimage.com/35x35/000/fff.png" alt="User Profile Image Dropdown"> 
            {{ Auth::user()->email }}
            <span class="caret"></span>
    </button>

<ul class="dropdown-menu" aria-labelledby="user-dropdown">
    <li><a href="{{ action('UserController@showProfile', ['user' => Auth::user()->slug]) }}">My Profile</a></li>
    @if (Auth::user()->isCandidate())
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
    @elseif (Auth::user()->isBusiness())
        <li><a href="{{ action('AdController@index', ['user' => Auth::user()->slug]) }}">Manage Ads</a></li>
        <li><a href="#">View Ads</a></li>
        <li><a href="#">Search Candidates</a></li>
        <li><a href="#">Messages</a></li>
    @endif
    <li role="separator" class="divider"></li>
    <li><a href="{{ action('Auth\AuthController@logout') }}">Logout</a></li>
</ul>

</div>

