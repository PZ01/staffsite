<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Staff Seeker</title>
  <meta name="description" content="Staff Seeker">
  <meta name="author" content="PZ01">
  <meta name="base_url" content="{{ URL::to('/') }}">
  @include('globals.styles')
</head>

<body>
    <div class="container">
        <div class="clearfix">
            <nav>
                <ul class="nav nav-pills pull-right">
                    @if (Auth::check())
                    <li role="presentation">
                        @include('layouts.user_dropdown')
                    </li>
                    @else
                    <li role="presentation"><a class="bootstrap-modal-form-open" href="#login" data-toggle="modal" data-target="#loginModal">Login</a></li>
                    <li role="presentation"><a class="bootstrap-modal-form-open" href="#register" data-toggle="modal" data-target="#registerModal">Register</a></li>
                    @endif
                </ul>
            </nav>

            <h3 class="text-muted"><a href="/">Staff Seeker</a></h3>
        </div>
        <hr/>

        @yield('content')  

        <footer class="footer">
            <p>&copy; 2015 Company, Inc.</p>
        </footer>

    </div> <!-- /container -->

    @include('pages.registerModal', [ 'modalId' => 'registerModal', 'modalTitle' => 'Register a new Account', 'size' => 'lg' ])
    @include('pages.loginModal', [ 'modalId' => 'loginModal', 'modalTitle' => 'Please Sign-In', 'size' => 'sm' ])

    @include('globals.scripts')
</body>

</html>
