<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Staff Seeker</title>
  <meta name="description" content="Staff Seeker">
  <meta name="author" content="PZ01">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>

<body>
    <div class="container-fluid">
        @yield('content')  
    </div> <!-- /container -->
</body>

</html>
