<div id="{{ $modalId }}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-{{ $size }}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ $modalTitle }}</h4>
      </div>
      @yield($modalId . 'Content')
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
