@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h2>{{ $ad->title }}</h2>
    <a class="btn btn-primary pull-right" href="{{ URL::previous() }}" role="button">Back</a>
    </div>

</div>

@endsection
