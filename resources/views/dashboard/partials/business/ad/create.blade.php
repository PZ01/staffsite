@extends('layouts.master')

@section('scripts')
<script src="{{ elixir('js/create_ad.js') }}"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h2>Post an Ad
            <br/><small>*An ad lasts two weeks and consumes one staff coin.</small>
        </h2>
        <form method="POST" action="{{ action('AdController@store', [ 'user' => Auth::user()->slug ]) }}">
            {{ csrf_field() }}
            <div class="form-group @if ($errors->has('title')) has-error @endif">
              <label for="usr">Title: 
                  <span data-toggle="tooltip" title="The main headline for your job ad." class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> 
              </label>
              <input type="text" name="title" class="form-control" id="ad-title">
              @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
            </div>

            <div class="form-group @if ($errors->has('jobTitleId')) has-error @endif">
                <label for="ad-job-title">Select Job Title:
                  <span data-toggle="tooltip" title="This is the position you are seeking to fill." class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> 
                </label>
                <select name="jobTitleId" class="form-control" id="ad-job-title">
                    @foreach (\App\JobTitle::all() as $jobTitle)
                        <option value="{{ $jobTitle->id }}"> {{ $jobTitle->display_name }} </option>
                    @endforeach
                </select>
                @if ($errors->has('jobTitleId')) <p class="help-block">{{ $errors->first('jobTitleId') }}</p> @endif
            </div>

            <div class="form-group @if ($errors->has('employmentTypeId')) has-error @endif">
                <label for="ad-employment-type">Select Employment Type: 
                  <span data-toggle="tooltip" title="The shift type you expect for the position offered." class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> 
                </label>
                <select name="employmentTypeId" class="form-control" id="ad-employment-type">
                    @foreach (\App\EmploymentType::all() as $employmentType)
                        <option value="{{ $employmentType->id }}"> {{ $employmentType->display_name }} </option>
                    @endforeach
                </select>
                @if ($errors->has('employmentTypeId')) <p class="help-block">{{ $errors->first('employmentTypeId') }}</p> @endif
            </div>

            <div class="form-group @if ($errors->has('description')) has-error @endif">
                <label for="ad-description">Description:
                  <span data-toggle="tooltip" title="This section outlines all the requirements for the position." class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> 
                </label>
                <textarea name="description" class="form-control" rows="5" id="ad-description"></textarea>
                @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
            </div>

            <div class="form-group">
                <label for='map'>Address</label>
                <map height="500px"></map>
            </div>

            <div class="btn-toolbar">
                <a class="btn btn-danger pull-right" href="{{ URL::previous() }}" role="button">Cancel</a>
                <input class="btn btn-success btn-primary pull-right submit-btn" type="submit" value="Submit">
            </div>
        </form>
    </div>
</div> <!-- End row -->
@endsection
