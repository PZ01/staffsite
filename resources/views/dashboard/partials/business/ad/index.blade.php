@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h2>My Ads</h2>
        <form id="search">
            <input type="text" placeholder="Search Ads" class="form-control" name="query" v-model="filterKey">
        </form>

        <grid
            :data="{{ \App\Ad::filteredAds(Auth::user()->userable_id) }}"
            :columns="{{ \App\Ad::publicColumnTitles() }}"
            :filter-key="filterKey"
            current-uri="{{ action('AdController@index', ['user' => Auth::user()->slug]) }}">
        </grid>    

        <form method="get">
            <button type="submit" formaction="{{ action('AdController@create', ['user' => Auth::user()->slug]) }}" class="pull-right top-buffer-sm btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Post New Job Ad
            </button>
        </form>
    </div>
</div>

@endsection
