<div class="text-right">
    @if ($isCurrentUser)
        <a href="{{ url()->current() . '/edit' }}">Edit My Business Page</a>
    @endif
</div>

<div class="form-group">
    <p> Business Name: {{ $user->present()->formattedName }} </p>
</div>

