<h3>
    <small><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></small>
    <a data-target="#languageList" data-toggle="collapse">
        Languages
    </a>
</h3>

<div id="languageList" class="collapse">
    <div class="col-sm-12">
        <div class="form-group">
            <input 
                class="form-control search" 
                placeholder="Search Languages" />

            <div class="text-right checkbox">
                <label>
                    <input id="showAllLanguages" type="checkbox" checked>
                    Show All
                </label>
            </div>
        </div>
    </div>

    <ul class="list-group list">
        @foreach (\App\Language::all() as $language)
            @set('checkIfSpeaksGivenLanguage', ($user->userable->speaksGivenLanguage($language->iso_code)) ? 'checked' : '')
            <li class="list-group-item">
                <div class="checkbox">
                    <label>
                        <input name="languages[]" type="checkbox" value="{{ $language->iso_code }}" {{ $checkIfSpeaksGivenLanguage }}>
                        <p class="languageName formCheckBoxLabel">{{ $language->display_name }}</p>
                    </label>
                </div>
                <p class="languageKnown" style="display:none;">{{ $checkIfSpeaksGivenLanguage }}</p>
                <p class="isoCode" style="display:none;">{{ $language->iso_code }}</p>
            </li>
        @endforeach
    </ul>
</div>

