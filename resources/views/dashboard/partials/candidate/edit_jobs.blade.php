<h3>
    <small><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></small>
    <a data-target="#collapse-experience" data-toggle="collapse">
        Experience
    </a>
</h3>
<div id="collapse-experience" class="collapse">
    <input id="current-job-hidden-input" type="hidden" name="currentJobId" value="{{ $user->userable->current_job_id }}"> 
    @foreach ($user->userable->jobs as $job)
    @set('expId', $job->id)
    <input type="hidden" name="jobs[{{ $expId }}][id]" value="{{ $expId }}"> 

    <div class="form-group">
        <label for="job-title" class="col-sm-2 control-label">Title</label>
        <div class="col-sm-10">
            <select name="jobs[{{ $expId }}][jobTitle]" class="form-control" id="job-title">
                @foreach (\App\JobTitle::all() as $jobTitle)
                @set('selectedJobTitle', ($jobTitle == $job->jobTitle) ? 'selected="selected"' : '')
                <option value="{{ $jobTitle->id }}" {{ $selectedJobTitle }}> {{ $jobTitle->display_name }} </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Employer</label>
        <div class="col-sm-10">
            <input type="text" name="jobs[{{ $expId }}][employer]" value="{{ $job->employer }}" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-2">
            <label for="startDate" class="control-label">Begin/End</label>
        </div>
        <div class="col-sm-10">
            <div class="input-group input-job-daterange">
                <input type="text" 
                       name="jobs[{{ $expId }}][beginDate]" 
                       value="{{ $job->present()->formattedBeginDate() }}"
                       class="input-sm form-control" 
                       readonly>
                <span class="input-group-addon">to</span>
                <input type="text" 
                       name="jobs[{{ $expId }}][endDate]" 
                       value="{{ $job->present()->formattedEndDate() }}"
                       class="input-sm form-control" 
                       readonly>
            </div>                                    
        </div>
    </div> 

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
            <textarea name="jobs[{{ $expId }}][description]" class="form-control" rows="5">{{ $job->description }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="btn-toolbar current-job-btns">
                @set('currJobClasses', ($job->id == $user->userable->current_job_id) ? 'btn btn-xs btn-success pull-right current-job-selected' : 'btn btn-xs btn-default pull-right')
                <button class="{{ $currJobClasses}}" type="button" value="{{ $expId }}">
                    I Currently Work Here
                </button>
                <button class="btn btn-xs btn-danger pull-right" type="button">Remove Job</button>
            </div>
        </div>
    </div>
    <hr>
    @endforeach
</div> 
