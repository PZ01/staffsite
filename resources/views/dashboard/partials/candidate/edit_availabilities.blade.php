<h3>
    <small><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></small>
    <a data-target="#collapse-availability" data-toggle="collapse">
        Availabilities
    </a>
</h3>

<div id="collapse-availability" class="collapse">
    <table name="availabilityTable" class="table table-striped">
        <tr>
            <th>Day Of Week</th>
            @foreach (\App\Shift::SHIFT_TYPES as $shiftName => $shiftValue)
                <th>
                    {{ $shiftName }}
                </th>
            @endforeach
        </tr>
        @foreach (\App\DayOfWeek::all() as $dayOfWeek)
            <tr>
                <td>
                    {{ $dayOfWeek->display_name }}
                </td>
                @foreach (\App\Shift::SHIFT_TYPES as $shiftName => $shiftValue)
                    @set('candidateAvailableForShift', ($user->userable->isAvailableForShift($dayOfWeek->id, $shiftValue)) ? 'checked' : '')
                    <td>
                        <input 
                        name="availabilities[]" 
                        type="checkbox" 
                        value="{{ $dayOfWeek->id . '_' . $shiftValue }}"
                        {{ $candidateAvailableForShift }}>
                    </td>
                @endforeach
            </tr>
        @endforeach
    </table>
</div>
