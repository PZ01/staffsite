<h3>
    <small><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></small>
    <a data-target="#collapse-general" data-toggle="collapse">
        General
    </a>
</h3>
<div id="collapse-general" class="collapse in col-sm-12">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" 
                   name="candidate[name]" 
                   value="{{ old('name', $user->name) }}"
                   class="form-control">
       </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Birthday</label>
        <div class="col-sm-10">
            <div class="date input-group">
                <input type="text" 
                       name="candidate[birthday]"
                       value="{{ old('birthday', $user->userable->present()->formattedBirthday) }}"
                       class="form-control" 
                       readonly>
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
        </div>
    </div>

    <div class="form-group">
        @set('isMale', $user->userable->isMale()) 
        <label for="genderGroup" class="col-sm-2 control-label">Gender</label><br/>
        <div class="col-sm-10 btn-group" data-toggle="buttons">
            <label class="btn btn-primary {{ ($isMale) ? 'active' : '' }}">
                <input type="radio" name="candidate[gender]" id="gender1" autocomplete="off" value="M" {{ ($isMale) ? 'checked' : '' }}>Male
            </label>
            <label class="btn btn-primary {{ ($isMale) ? '' : 'active' }}">
                <input type="radio" name="candidate[gender]" id="gender2" autocomplete="off" value="F" {{ ($isMale) ? '' : 'checked' }}>Female
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <input name="candidate[hasDriversLicense]" type="checkbox" {{ ($user->userable->has_drivers_license) ? 'checked' : '' }}>
                    <p class="formCheckBoxLabel">I own a driver's license.</p>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="candidate[ownsCar]" type="checkbox" {{ ($user->userable->owns_car) ? 'checked' : '' }}>
                    <p class="formCheckBoxLabel">I own a car.</p>
                </label>
            </div>
        </div>
    </div>

    <label>Address</label>
    <map :address="{{ $user->userable->address }}" height="500px"></map>
</div>
