<div class="text-right">
    @if ($isCurrentUser)
        <a href="{{ url()->current() . '/edit' }}">Edit My Profile</a>
    @endif
</div>

<div class="row">
    <div class="col-sm-6 nopadding-0">
        <img src="http://blog.eclipsestores.com/wp-content/uploads/2016/01/Poster-Sample-2-150x300.jpg" class="img-responsive pull-right">
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12 nopadding-0">
                <img src="https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c0.90.720.720/13277521_550716801755981_520824958_n.jpg" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 nopadding-0">
                <img src="http://www.beautytipsmart.com/wp-content/uploads/2014/07/Highlighted-Hairstyles-Ideas-For-Brunettes-Long-Short-Hair-150x150.jpg" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div class="row top-buffer">
    Profile Rating : 
    <div class="progress">
        <div 
            class="progress-bar progress-bar-striped" 
            role="progressbar" 
            aria-valuenow="{{ $user->userable->rate() }}" 
            aria-valuemin="0" 
            aria-valuemax="100" 
            style="min-width: 2em; width: {{ $user->userable->rate() }}%">
                {{ $user->userable->rate() }}%
        </div>
    </div>    
</div>

<div class="row">
    <div class="col-sm-11">
        <div class="form-group">
            <p> Name: {{ $user->present()->formattedName }} </p>
            <p> Age: {{ $user->userable->present()->formattedAge }} </p>
            <p> Sexe: {{  $user->userable->present()->formattedGender }} </p>
            <p> Driver's License: {{ $user->userable->present()->formattedDriversLicense }} </p>
            <p> Owns Car: {{ $user->userable->present()->formattedOwnsCar }} </p>

            @if ($user->userable->speaksLanguages())
            Spoken Languages: 
            <ul>
                    @foreach ($user->userable->spokenLanguages as $language)
                        <li>
                            <p> {{ $language->display_name }} </p>
                        </li>
                    @endforeach
            </ul>
            @endif

            @if ($user->userable->hasAvailabilities())
            Availabilities:
            <ul>
                @foreach ($user->userable->availabilities as $availability)
                    <li>
                        <p> {{ $availability->dayOfWeek->display_name }} : </p>
                        <ul>
                        @foreach ($availability->activeShifts()  as $activeShift)
                            <li>
                                {{ $activeShift }}
                            </li>
                        @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
            @endif

            @if ($user->userable->hasJobs())
            Jobs:
            <ul>
                @foreach ($user->userable->jobs as $job)
                    <li>
                        <p> Employer : {{ $job->employer }} </p>
                    </li>
                    <li>
                        <p> Job Title : {{ $job->jobTitle->display_name }} </p>
                    </li>
                    <li>
                        <p> Duration : {{ $job->duration() }} </p>
                    </li>
                @endforeach
            </ul>
            @endif

        </div>
    </div>
</div>

