@section('scripts')
<script src="{{ elixir('js/edit_candidate.js') }}"></script>
@endsection

<div class="form-group">
    <form method="POST" action="patch" class="form-horizontal" id="editCandidateProfile">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="row">
            <!-- TODO This will probably end up in the form once we handle image uploads. -->
            @include('dashboard.partials.candidate.edit_pictures')
            @include('dashboard.partials.candidate.edit_general')
            @include('dashboard.partials.candidate.edit_languages')
            @include('dashboard.partials.candidate.edit_availabilities')
            @include('dashboard.partials.candidate.edit_jobs')

            <div class="btn-toolbar">
                <a class="btn btn-danger pull-right" href="{{ URL::previous() }}" role="button">Cancel</a>
                <input class="btn btn-success btn-primary pull-right" type="submit" value="Save">
            </div>
        </div>
    </form>
</div>

<div class="row top-buffer">
    @include('globals.partials.errors')
</div>

