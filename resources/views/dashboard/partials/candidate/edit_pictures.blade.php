<h3>
    <small><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></small>
    <a data-target="#collapse-pictures" data-toggle="collapse">
        Pictures
    </a>
</h3>
<div id="collapse-pictures" class="collapse">
    <div class="col-sm-6 nopadding-0">
        <img src="http://blog.eclipsestores.com/wp-content/uploads/2016/01/Poster-Sample-2-150x300.jpg" class="img-responsive pull-right">
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12 nopadding-0">
                <img src="https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c0.90.720.720/13277521_550716801755981_520824958_n.jpg" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 nopadding-0">
                <img src="http://www.beautytipsmart.com/wp-content/uploads/2014/07/Highlighted-Hairstyles-Ideas-For-Brunettes-Long-Short-Hair-150x150.jpg" class="img-responsive">
            </div>
        </div>
    </div>
</div>
