@extends('layouts.master')

@section('content')

@set('viewPath', ($user->isCandidate()) ? 'dashboard.partials.candidate.' : 'dashboard.partials.business.') 
@if (isset($edit) && $edit == true)
    @include($viewPath . 'edit')
@else
    @include($viewPath . 'profile')
@endif

@endsection
