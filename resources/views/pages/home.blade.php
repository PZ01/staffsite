@extends('layouts.master')

@section('content')
@if (Session::has('flash_message'))
<alert type="{{ Session::get('flash_message_level') }}"><strong>{{ Session::get('flash_message_header') }}</strong> {{ Session::get('flash_message') }} </alert>
@endif
    <p> Home page </p>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@endsection
