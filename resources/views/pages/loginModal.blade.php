@extends('layouts.modal')

@section('loginModalContent')
<form method="POST" action="login" class="bootstrap-modal-form">
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="form-group">
            <label for="inputEmail" class="sr-only">Email Address</label>
            <input name="email"
                   type="email"
                   id="inputEmail"
                   class="form-control"
                   placeholder="Email address"
                   required
                   autofocus>
        </div>
    </div>

    <div class="modal-footer">
        <input class="btn btn-default btn-primary" type="submit" value="Sign In">
        <button class="btn modal-default-button" data-dismiss="modal">Back</button>
    </div>
</form>
@endsection

