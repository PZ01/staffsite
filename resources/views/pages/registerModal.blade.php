@extends('layouts.modal')

@section('registerModalContent')
<form method="POST" action="register" class="bootstrap-modal-form">
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="form-group">
            <label for="email" class="sr-only">E-Mail Address</label>
            <input name="email" type="email" class="form-control" placeholder="Email Address" required autofocus>
        </div>

        <div class="radio">
          <label>
            <input name="user_type" type="radio" value="candidate" checked>
            I am a candidate looking for work!
          </label>
        </div>

        <div class="radio">
          <label>
            <input name="user_type" type="radio" value="business">
            I am a business interested in hiring candidates.
          </label>
        </div>
    </div>

    <div class="modal-footer">
        <input type="submit" value="Register" class="btn btn-default btn-primary">
        <button class="btn modal-default-button" data-dismiss="modal">Back</button>
    </div>
</form>
@endsection

