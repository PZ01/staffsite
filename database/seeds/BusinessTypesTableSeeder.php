<?php

use Illuminate\Database\Seeder;

class BusinessTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_types')->delete();
        DB::table('business_types')->insert([
            ['display_name' => 'Coffee Shop'],
            ['display_name' => 'Restaurant'],
            ['display_name' => 'Caterer'],
            ['display_name' => 'Bar, Tavern, Pub'],
            ['display_name' => 'Supper Club'],
            ['display_name' => 'Night Club'],
            ['display_name' => 'Theater'],
            ['display_name' => 'After Hour'],
            ['display_name' => 'Lodging'],
            ['display_name' => 'Other'],
        ]);
    }
}
