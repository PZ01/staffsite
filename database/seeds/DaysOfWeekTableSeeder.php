<?php

use Illuminate\Database\Seeder;

class DaysOfWeekTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days_of_week')->delete();
        DB::table('days_of_week')->insert([
            ['display_name' => 'Monday'],
            ['display_name' => 'Thuesday'],
            ['display_name' => 'Wednesday'],
            ['display_name' => 'Thursday'],
            ['display_name' => 'Friday'],
            ['display_name' => 'Saturday'],
            ['display_name' => 'Sunday'],
        ]);
    }
}
