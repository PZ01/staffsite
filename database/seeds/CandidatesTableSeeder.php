<?php

use Illuminate\Database\Seeder;

class CandidatesTableSeeder extends Seeder
{
    const CANDIDATE_COUNT = 10; 
    const CANDIDATE_LANGUAGE_COUNT = 3; 
    const CANDIDATE_MIN_AVAILABILITIES = 3; 
    const CANDIDATE_MAX_AVAILABILITIES = 5; 
    const CANDIDATE_JOB_COUNT = 3; 

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->delete();
        DB::table('candidate_language')->delete();

        factory(App\User::class, App\Candidate::class, CandidatesTableSeeder::CANDIDATE_COUNT)
            ->create()
            ->each(function ($u) {
                $this->addSpokenLanguages($u, CandidatesTableSeeder::CANDIDATE_LANGUAGE_COUNT); 
                $this->addAvailabilities($u); 
                $this->addJobs($u, CandidatesTableSeeder::CANDIDATE_JOB_COUNT); 
                $this->addAddress($u);
            });
    }

    protected function addSpokenLanguages(App\User $user, $languageCount)
    {
        $isoCodes = App\Language::pluck('iso_code');
        $isoCodeIndexes = [];
        $faker = Faker\Factory::create();

        for ($i = 0; $i < $languageCount; $i++) {
            $isoCodeIndexes []= $faker->unique()->numberBetween(0, $isoCodes->count() - 1);
            $user->userable->spokenLanguages()->attach($isoCodes[$isoCodeIndexes[$i]]);
        }
    }

    protected function addAvailabilities(App\User $user)
    {
        $dayOfWeekIds = App\DayOfWeek::pluck('id');
        $dayOfWeekIndexes = [];
        $faker = Faker\Factory::create();
        $count = $faker->numberBetween(CandidatesTableSeeder::CANDIDATE_MIN_AVAILABILITIES, CandidatesTableSeeder::CANDIDATE_MAX_AVAILABILITIES);

        for ($i = 0; $i < $count; $i++) {
            $dayOfWeekIndexes []= $faker->unique()->numberBetween(0, $dayOfWeekIds->count() - 1);
            $avail = new App\Availability();
            $avail->day_of_week_id = $dayOfWeekIds[$dayOfWeekIndexes[$i]];
            $avail->shifts = $faker->numberBetween(1, App\Shift::shiftCombinations() - 1);
            $user->userable->availabilities()->save($avail);
        }
    }

    protected function addJobs(App\User $user, $jobCount)
    {
        $faker = Faker\Factory::create();
        $randomJobTitleId = App\JobTitle::inRandomOrder()->value('id');

        for ($i = 0; $i < $jobCount; $i++) {
            $fakeDate = $faker->date($format = 'Y-m-d', $max = 'now');
            $job = new App\Job();
            $job->job_title_id = $randomJobTitleId;
            $job->begin_date = \Carbon\Carbon::today()->subYears(5 + $i);
            $job->end_date = $job->begin_date->addYears($faker->numberBetween(1, 3));
            $job->employer = $faker->company();
            $job->description = $faker->text(250);
            $user->userable->jobs()->save($job);
        }

        $randomJobIndex = $faker->numberBetween(0, $jobCount - 1);
        $randomJobId = $user->userable->jobs->pluck('id')[$randomJobIndex];
        $user->userable->current_job_id = $randomJobId;
        $user->userable->save();
    }

    protected function addAddress(App\User $user)
    {
        $faker = Faker\Factory::create();
        $address = new App\Address([
            'house_number' => $faker->buildingNumber(),
            'street' => $faker->streetName(),
            'locality' => $faker->city(),
            'region' => $faker->state(),
            'display_name' => $faker->address(),
            'latitude' => $faker->latitude(),
            'longitude' => $faker->longitude(),
            'country_iso_code' => 'USA'
        ]);

        $user->userable->saveAddress($address);
    }

}

