<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        DB::table('permissions')->insert([
            [
                'name' => 'view_public_rofiles',
                'display_name' => 'View Public Profiles',
                'description' => 'View public profiles of Businesses and other Candidates.'
            ]
        ]);
    }
}
