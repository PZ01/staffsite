<?php

use Illuminate\Database\Seeder;

class BusinessesTableSeeder extends Seeder
{
    const BUSINESS_COUNT = 10; 

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('businesses')->delete();
        factory(App\User::class, App\Business::class, BusinessesTableSeeder::BUSINESS_COUNT)->create();
    }
}
