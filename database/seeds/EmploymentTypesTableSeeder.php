<?php

use Illuminate\Database\Seeder;

class EmploymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employment_types')->delete();
        DB::table('employment_types')->insert([
            ['display_name' => 'Part-Time'],
            ['display_name' => 'Full-Time'],
            ['display_name' => 'On Call'],
        ]);
    }
}
