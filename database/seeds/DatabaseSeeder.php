<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Order is relevant
        //
        
        // Static Table Initialization
        $this->call(LanguagesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(DaysOfWeekTableSeeder::class);
        $this->call(JobTitlesTableSeeder::class);
        $this->call(BusinessTypesTableSeeder::class);
        $this->call(BusinessAmbiancesTableSeeder::class);
        $this->call(EmploymentTypesTableSeeder::class);

        // Dynamic Table Initialization
        $this->call(CandidatesTableSeeder::class);
        $this->call(BusinessesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
    }
}
