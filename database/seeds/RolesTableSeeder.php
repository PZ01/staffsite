<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('roles')->insert([
            [
                'name' => 'candidate',
                'display_name' => 'Candidate',
                'description' => 'User is interested in job offers.'
            ],
            [
                'name' => 'business',
                'display_name' => 'Business',
                'description' => 'User owns an establishement. He is interested in recruiting staff.'
            ]
        ]);
    }
}
