<?php

use Illuminate\Database\Seeder;

class BusinessAmbiancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_ambiances')->delete();
        DB::table('business_ambiances')->insert([
            ['display_name' => 'Family Friendly'],
            ['display_name' => 'Festive'],
            ['display_name' => 'Chic'],
            ['display_name' => '18+'],
            ['display_name' => '21+'],
            ['display_name' => 'Young & Trendy'],
            ['display_name' => 'Busy'],
        ]);
    }
}
