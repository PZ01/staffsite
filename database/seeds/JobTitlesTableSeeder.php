<?php

use Illuminate\Database\Seeder;

class JobTitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_titles')->delete();
        DB::table('job_titles')->insert([
            ['display_name' => 'Apprentice Bartender'],
            ['display_name' => 'Area Director'],
            ['display_name' => 'Assistant Chef'],
            ['display_name' => 'Assistant General Manager'],
            ['display_name' => 'Assistant Kitchen Manager'],
            ['display_name' => 'Associate Creative Director'],
            ['display_name' => 'Baker'],
            ['display_name' => 'Bakery-Cafe Associate'],
            ['display_name' => 'Barback'],
            ['display_name' => 'Barista'],
            ['display_name' => 'Bar Manager'],
            ['display_name' => 'Bartender'],
            ['display_name' => 'Brand Manager'],
            ['display_name' => 'Bus Person'],
            ['display_name' => 'Cashier'],
            ['display_name' => 'Casual Restaurant Manager'],
            ['display_name' => 'Chef'],
            ['display_name' => 'Chef Manager'],
            ['display_name' => 'Coffee Tasting Room Assistant'],
            ['display_name' => 'Communications Manager'],
            ['display_name' => 'Cook'],
            ['display_name' => 'Culinary Services Supervisor'],
            ['display_name' => 'Culinary Trainee'],
            ['display_name' => 'Dessert Finisher'],
            ['display_name' => 'Digital Marketing Manager'],
            ['display_name' => 'Dining Room Manager'],
            ['display_name' => 'Director of Human Resources'],
            ['display_name' => 'Dishwasher'],
            ['display_name' => 'District Manager'],
            ['display_name' => 'Espresso Beverage Maker'],
            ['display_name' => 'Executive Chef'],
            ['display_name' => 'Expeditor'],
            ['display_name' => 'Field Recruiting Manager'],
            ['display_name' => 'Fine Dining Restaurant Manager'],
            ['display_name' => 'Food Runner'],
            ['display_name' => 'Front Manager'],
            ['display_name' => 'Grill Cook'],
            ['display_name' => 'Hibachi Chef'],
            ['display_name' => 'Host'],
            ['display_name' => 'Human Resources Manager'],
            ['display_name' => 'Inventory Analyst'],
            ['display_name' => 'Kitchen Manager'],
            ['display_name' => 'Kitchen Worker'],
            ['display_name' => 'Lead Cook'],
            ['display_name' => 'Line Cook'],
            ['display_name' => 'Manager, Research and Development'],
            ['display_name' => 'National Training Manager'],
            ['display_name' => 'Operations Analyst'],
            ['display_name' => 'Pantry Worker'],
            ['display_name' => 'Prep Cook'],
            ['display_name' => 'Product Manager'],
            ['display_name' => 'Regional Brand Development Manager'],
            ['display_name' => 'Regional Facilities Manager'],
            ['display_name' => 'Regional Manager'],
            ['display_name' => 'Regional Operations Specialist'],
            ['display_name' => 'Restaurant General Manager'],
            ['display_name' => 'Restaurant Manager'],
            ['display_name' => 'Server'],
            ['display_name' => 'Shift Supervisor'],
            ['display_name' => 'Sous Chef'],
            ['display_name' => 'Steward'],
        ]);
    }
}
