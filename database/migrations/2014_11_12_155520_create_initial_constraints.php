<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function ($table) {
            $table->foreign('business_id')->references('id')->on('businesses');
            $table->foreign('job_title_id')->references('id')->on('job_titles');
            $table->foreign('employment_type_id')->references('id')->on('employment_types');
            $table->foreign('address_id')->references('id')->on('addresses');
        });

        Schema::table('addresses', function ($table) {
            $table->foreign('country_iso_code')->references('iso_code')->on('countries');
        });

        Schema::table('availabilities', function ($table) {
            $table->unique(['candidate_id', 'day_of_week_id', 'shifts']);
            $table->foreign('candidate_id')->references('id')->on('candidates');
            $table->foreign('day_of_week_id')->references('id')->on('days_of_week');
        });
        
        Schema::table('schedules', function ($table) {
            $table->unique(['ad_id', 'day_of_week_id', 'shifts']);
            $table->foreign('ad_id')->references('id')->on('ads');
            $table->foreign('day_of_week_id')->references('id')->on('days_of_week');
        });

        Schema::table('businesses', function ($table) {
            $table->foreign('business_type_id')->references('id')->on('business_types');
            $table->foreign('business_ambiance_id')->references('id')->on('business_ambiances');
        });

        Schema::table('candidates', function ($table) {
            $table->foreign('current_job_id')->references('id')->on('jobs');
            $table->foreign('address_id')->references('id')->on('addresses');
        });

        Schema::table('candidate_language', function ($table) {
            $table->foreign('candidate_id')->references('id')->on('candidates');
            $table->foreign('language_iso_code')->references('iso_code')->on('languages');
            $table->primary(['candidate_id', 'language_iso_code']);
        });

        Schema::table('jobs', function ($table) {
            $table->foreign('candidate_id')->references('id')->on('candidates');
            $table->foreign('job_title_id')->references('id')->on('job_titles');
        });

        Schema::table('login_tokens', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('photos', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
