<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialSchema extends Migration
{
    /**
     * Run the migrations. The table declaration order is
     * not important because the integrity constraints are
     * applied in a subsequent migration.
     * @return void
     */
    public function up()
    {
        /**
         * Static Table Declarations
         */
        Schema::create('business_ambiances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name', 50);
        });

        Schema::create('business_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name', 50);
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->string('iso_code', 3)->primary(); // ISO 3166-1 alpha-3
            $table->string('display_name', 100);
        });

        Schema::create('days_of_week', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name', 10);
        });

        Schema::create('languages', function (Blueprint $table) {
            $table->string('iso_code', 2)->primary(); // ISO 639-1
            $table->string('display_name', 100);
        });

        Schema::create('job_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name', 50);
        });

        Schema::create('employment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name', 50);
        });

        /**
         * Dynamic Tables
         */
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_id')->unsigned();
            $table->integer('job_title_id')->unsigned();
            $table->integer('employment_type_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->string('title', 50);
            $table->string('description');
            $table->date('expiry_date');
            $table->date('posted_date');
            $table->timestamps();
        });

        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('house_number');
            $table->string('street');
            $table->string('locality');
            $table->string('region');
            $table->string('postcode')->nullable();
            $table->string('display_name');
            $table->decimal('latitude',9, 6);
            $table->decimal('longitude', 9, 6);
            $table->string('country_iso_code', 3);
            $table->timestamps();
        });

        Schema::create('availabilities', function (Blueprint $table) {
            $table->integer('candidate_id')->unsigned();
            $table->integer('day_of_week_id')->unsigned();
            $table->tinyInteger('shifts')->unsigned();
        });

        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_type_id')->nullable()->unsigned();
            $table->integer('business_ambiance_id')->nullable()->unsigned();
            $table->string('website_url');
            $table->timestamps();
        });

        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->date('birthday')->nullable();
            $table->char('gender', 1);
            $table->text('description');
            $table->boolean('has_drivers_license');
            $table->boolean('owns_car');
            $table->integer('current_job_id')->nullable()->unsigned();
            $table->integer('address_id')->nullable()->unsigned();
            $table->char('country', 2); // ISO 3166-1 alpha-2 
            $table->timestamps();
        });

      
        Schema::create('candidate_language', function (Blueprint $table) {
            $table->integer('candidate_id')->unsigned();
            $table->string('language_iso_code', 2);
        });

        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id')->unsigned();
            $table->integer('job_title_id')->unsigned();
            $table->string('employer');
            $table->string('description', 250);
            $table->date('begin_date');
            $table->date('end_date');
            $table->timestamps();
        });

        Schema::create('login_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('token', 50)->index();
            $table->timestamps();
        });
        
        Schema::create('photos', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('user_id')->unsigned();
            $table->string('url');
        });

        Schema::create('schedules', function (Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('day_of_week_id')->unsigned();
            $table->tinyInteger('shifts')->unsigned();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userable_id')->unsigned()->index();
            $table->string('userable_type');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone', 42);
            $table->string('slug')->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('candidates');
        Schema::dropIfExists('businesses');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('business_types');
        Schema::dropIfExists('business_ambiances');
        Schema::dropIfExists('users');
        Schema::dropIfExists('login_tokens');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('photos');
        Schema::dropIfExists('candidate_language');
        Schema::dropIfExists('days_of_week');
        Schema::dropIfExists('availabilities');
        Schema::dropIfExists('schedules');
        Schema::dropIfExists('ads');
        Schema::dropIfExists('employment_types');
        Schema::dropIfExists('job_titles');
        Schema::dropIfExists('jobs');
        Schema::enableForeignKeyConstraints();
    }
}
