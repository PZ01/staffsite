<?php

use App\StringHelper;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------

The order of the following model factories is important.
*/

$factory->define(App\Candidate::class, function ($faker) {
    return [
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'gender' => $faker->randomElement($array = array ('M','F')),
        'has_drivers_license' => $faker->boolean(),
        'owns_car' => $faker->boolean(),
        'description' => $faker->sentence(15),
    ];
});

$factory->define(App\Business::class, function ($faker) {
    return [
        'business_type_id' => App\BusinessType::inRandomOrder()->value('id'),
        'business_ambiance_id' => App\BusinessAmbiance::inRandomOrder()->value('id'),
        'website_url' => $faker->url(),
    ];
});

$factory->define(App\User::class, function ($faker) {
    $userEmail = $faker->safeEmail;
    return [
        'name' => $faker->name,
        'email' => $userEmail,
        'remember_token' => str_random(10),
        'slug' => StringHelper::emailSlug($userEmail, App\User::count())
    ];
});

$factory->defineAs(App\User::class, App\Candidate::class, function ($faker) use ($factory) {
    $user = $factory->raw(App\User::class);

    return array_merge($user, [
        'userable_type' => App\Candidate::class,
        'userable_id' => function () {
            return factory(App\Candidate::class)->create()->id;
        },
    ]);
});

$factory->defineAs(App\User::class, App\Business::class, function ($faker) use ($factory) {
    $user = $factory->raw(App\User::class);

    return array_merge($user, [
        'name' => $faker->company(),
        'userable_type' => App\Business::class,
        'userable_id' => function () {
            return factory(App\Business::class)->create()->id;
        },
    ]);
});
