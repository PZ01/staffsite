<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelpersTest extends TestCase
{
    use DatabaseTransactions;

    /**********************
     * StringHelper Tests
     *********************/

    /** @test */
    public function create_slugs_based_on_valid_emails_which_are_unique()
    {
        $slug1 = App\StringHelper::emailSlug('dummy@example.com');
        $slug2 = App\StringHelper::emailSlug('#bang.$w33t@overload.com');
        $slug3 = App\StringHelper::emailSlug('%^#@gmail.com');

        $this->assertEquals('dummy', $slug1);
        $this->assertEquals('bang.w33t', $slug2);
        $this->assertTrue(is_int((int)$slug3));
    }

    /** @test */
    public function create_slug_based_on_valid_email_which_is_already_exists()
    {
        $testEmail = 'd_m_@oracle.com';
        $testSlug  = App\StringHelper::emailSlug($testEmail);

        factory(App\User::class)->create(['email' => $testEmail, 'slug' => $testSlug]);
        $slug = App\StringHelper::emailSlug($testEmail);

        $this->assertEquals('dm1', $slug);
    }
}
