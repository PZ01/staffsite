<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AvailabilityTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function check_that_candidate_has_expected_number_of_availabilities()
    {
        $candidate = $this->createCandidateWithFixedAvailability(pow(2, 3) - 1); 
        $availabilities = $candidate->availabilities; 
        $this->assertEquals(count($availabilities), 1);
    }

    /** @test */
    public function check_that_candidate_availability_has_correct_day_of_week()
    {
        $candidate = $this->createCandidateWithFixedAvailability(pow(2, 3) - 1); 
        $dayOfWeekName = $candidate->availabilities[0]->dayOfWeek->display_name; 
        $this->assertEquals('Monday', $dayOfWeekName);
    }

    /** @test */
    public function check_that_candidates_have_expected_active_shifts()
    {
        $candidate = $this->createCandidateWithFixedAvailability(pow(2, 3) - 1); 
        $candidateShifts = $candidate->availabilities[0]->activeShifts();

        $this->assertEquals(count($candidateShifts), 3);
        foreach (App\Shift::SHIFT_TYPES as $key => $val) {
            $this->assertTrue(in_array($key, $candidateShifts));
        }

        $candidate2 = $this->createCandidateWithFixedAvailability(pow(2, 2) - 1); 
        $candidate2Shifts = $candidate2->availabilities[0]->activeShifts();

        $this->assertEquals(count($candidate2Shifts), 2);
        $this->assertTrue(in_array('AM', $candidate2Shifts));
        $this->assertTrue(in_array('PM', $candidate2Shifts));
   }

    /** @test */
    public function set_shift_on_given_availability_to_a_candidate()
    {
        $candidate = $this->createCandidateWithFixedAvailability(pow(2, 3) - 1); 
        $candidate->availabilities()->first()->setShift(App\Shift::SHIFT_TYPES['AM']);
        $candidateShifts = $candidate->availabilities->first()->activeShifts();

        $this->assertEquals(count($candidateShifts), 1);
        $this->assertTrue(in_array('AM', $candidateShifts));

        $candidate->availabilities()->first()->setShift(App\Shift::SHIFT_TYPES['AM'], App\Shift::SHIFT_TYPES['Night']);
        $candidateShifts = $candidate->availabilities()->first()->activeShifts();

        $this->assertEquals(count($candidateShifts), 2);
        $this->assertTrue(in_array('AM', $candidateShifts));
        $this->assertTrue(in_array('Night', $candidateShifts));
        $this->assertTrue(! in_array('PM', $candidateShifts));
    }

    /** @test */
    public function check_if_shift_is_set_on_candidate_availability()
    {
        $candidate = $this->createCandidateWithFixedAvailability(0); 
        $this->assertFalse($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['AM']));
        $this->assertFalse($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['PM']));
        $this->assertFalse($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['Night']));

        $candidate = $this->createCandidateWithFixedAvailability(1); 
        $this->assertTrue($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['AM']));
        $this->assertFalse($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['PM']));
        $this->assertFalse($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['Night']));

        $candidate = $this->createCandidateWithFixedAvailability(5); 
        $this->assertTrue($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['AM']));
        $this->assertFalse($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['PM']));
        $this->assertTrue($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['Night']));
    }

    /** @test */
    public function check_if_candidate_availability_shift_total_can_be_changed()
    {
        $candidate = $this->createCandidateWithFixedAvailability(0); 
        $candidate->availabilities()->first()->setShiftTotal([1, 2, 4]);

        $this->assertTrue($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['AM']));
        $this->assertTrue($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['PM']));
        $this->assertTrue($candidate->availabilities()->first()->shiftIsSet(App\Shift::SHIFT_TYPES['Night']));
    }

    /** @test */
    public function try_converting_different_shift_arrays_using_the_availability_utility_function_and_make_sure_they_are_valid()
    {
        $validCombination = [App\Shift::SHIFT_TYPES['AM'],  App\Shift::SHIFT_TYPES['PM'], App\Shift::SHIFT_TYPES['Night']];
        $invalidCombination0 = [10, 10, 10];
        $invalidCombination1 = [App\Shift::SHIFT_TYPES['AM'], App\Shift::SHIFT_TYPES['AM']];
        $invalidCombination2 = [ 1, 2, 3];

        $this->assertEquals(7, App\Shift::convertShiftArrayToSum($validCombination));
        $this->assertEquals(-1, App\Shift::convertShiftArrayToSum($invalidCombination0));
        $this->assertEquals(-1, App\Shift::convertShiftArrayToSum($invalidCombination1));
        $this->assertEquals(-1, App\Shift::convertShiftArrayToSum($invalidCombination2));
    }

    public function createCandidateWithFixedAvailability($availableShifts)
    {
        $user = factory(App\User::class, App\Candidate::class)->create();
        $availability = new App\Availability();
        $availability->day_of_week_id = App\DayOfWeek::first()->id;
        $availability->shifts = $availableShifts;
        $user->userable->availabilities()->save($availability);

        return $user->userable;
    }
}
