<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function fetch_user_by_his_email()
    {
        $email = 'user@mail.com';
        $user = factory(App\User::class)->create(['email' => $email]);
         
        $this->assertEquals($user->id, App\User::byEmail($email)->id);
    }

    /** @test */
    public function check_if_our_db_has_a_given_email()
    {
        $email = 'user@mail.com';
        $user = factory(App\User::class)->create(['email' => $email]);
         
        $this->assertEquals(true, App\User::emailExists($email));
        $this->assertEquals(false, App\User::emailExists('nonexistant@not.com'));
    }

    /** @test */
    public function check_if_user_is_a_candidate()
    {
        $user = factory(App\User::class)->create();
        $candidate = factory(App\User::class, App\Candidate::class)->create();

        $this->assertEquals(false, $user->isCandidate());
        $this->assertEquals(true, $candidate->isCandidate());
    }

    /** @test */
    public function check_if_user_is_a_business()
    {
        $user = factory(App\User::class)->create();
        $business = factory(App\User::class, App\Business::class)->create();

        $this->assertEquals(false, $user->isBusiness());
        $this->assertEquals(true, $business->isBusiness());
    }
}
