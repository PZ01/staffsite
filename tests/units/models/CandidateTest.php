<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class CandidateTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function check_if_user_speaks_provided_language()
    {
        $candidate = App\User::where('userable_type', 'App\Candidate')->first()->userable;
        
        $speaksLanguage = $candidate->speaksGivenLanguage($candidate->spokenLanguages()->first()->iso_code);
        $this->assertTrue($speaksLanguage);

        $speaksLanguage = $candidate->speaksGivenLanguage('zz'); // 'zz' is a non-existant iso-code
        $this->assertFalse($speaksLanguage);
    }

    /** @test */
    public function check_if_user_has_maximum_number_of_jobs_allowed()
    {
        // Default seeded job-count is three
        $this->assertEquals(CandidatesTableSeeder::CANDIDATE_JOB_COUNT, 3);

        $candidate = App\User::where('userable_type', 'App\Candidate')->first()->userable;
        $candidate->current_job_id = null;
        $candidate->save();
        
        // User should have maximum number allowed
        $this->assertTrue($candidate->hasMaxAmountOfJobs());

        $candidate->jobs()->first()->delete();

        // User should have space for one more job
        $this->assertFalse($candidate->hasMaxAmountOfJobs());
    }

}
