<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function register_a_new_user_as_a_candidate_with_correct_input()
    {
        $registrationEmail = 'dummy@mail.com';
        $this->visit('/')
             ->click('Register')
             ->seePageIs('/')
             ->type($registrationEmail, 'email')
             ->select('candidate', 'user_type')
             ->press('Register')
             ->seeInDatabase('users', ['email' => $registrationEmail, 'userable_type' => App\Candidate::class]);
    }

    /** @test */
    public function register_a_new_user_as_a_business_with_correct_input()
    {
        $registrationEmail = 'dummy@mail.com';
        $this->visit('/')
             ->click('Register')
             ->seePageIs('/')
             ->type($registrationEmail, 'email')
             ->select('business', 'user_type')
             ->press('Register')
             ->seeInDatabase('users', ['email' => $registrationEmail, 'userable_type' => App\Business::class]);
    }

    /** @test */
    public function register_a_new_user_as_a_business_with_incorrect_email()
    {
        $registrationEmail = 'dummy.mail.com';
        $this->visit('/')
             ->click('Register')
             ->seePageIs('/')
             ->type($registrationEmail, 'email')
             ->select('business', 'user_type')
             ->press('Register')
             ->see('The email must be a valid email address.');
    }

    /** @test */
    public function register_a_new_user_as_a_candidate_with_incorrect_email()
    {
        $registrationEmail = 'dummy.mail.com';
        $this->visit('/')
             ->click('Register')
             ->seePageIs('/')
             ->type($registrationEmail, 'email')
             ->select('candidate', 'user_type')
             ->press('Register')
             ->see('The email must be a valid email address.');
    }
}

