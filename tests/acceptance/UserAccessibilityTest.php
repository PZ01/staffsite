<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserAccessibilityTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function make_sure_an_authenticated_candidate_can_visit_his_profile()
    {
        $user = App\User::where('userable_type', 'App\Candidate')->first();
        $this->actingAs($user)
             ->visit('/profile/' . $user->slug)
             ->see($user->name)
             ->see($user->userable->gender)
             ->see($user->userable->present()->formattedAge);
    }

    /** @test */
    public function make_sure_that_an_unauthanticated_user_cannot_visit_profile_pages()
    {
        $user = App\User::first();
        $this->visit('/profile/' . $user->slug)
             ->seePageIs('/');
    }

    /** @test */
    public function make_sure_that_an_authenticated_candidate_cannot_edit_another_candidates_profile()
    {
        $user = App\User::where('userable_type', 'App\Candidate')->first();
        $lastUser = App\User::all()->where('userable_type', 'App\Candidate')->last();
        $this->actingAs($user)
             ->visit('/profile/' . $lastUser->slug)
             ->see($lastUser->name)
             ->see($lastUser->userable->gender)
             ->see($lastUser->userable->present()->formattedAge)
             ->dontSee('Edit My Profile');
    }
}
