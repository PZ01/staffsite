<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function login_with_an_existing_account()
    {
        $this->json('POST', 'login', ['email' => App\User::first()->email])
             ->seeJson(App\JsonHelper::success());
    }

    /** @test */
    public function login_with_a_non_existant_account()
    {
        $this->json('POST', 'login', ['email' => 'hello@world.xxx'])
            ->seeJson([
                "email" => ["The selected email is invalid."]
            ]);
    }

    /** @test */
    public function login_with_an_invalid_email()
    {
        $this->json('POST', 'login', ['email' => 'hello.world.xxx'])
            ->seeJson([
                "email" => ["The email must be a valid email address."]
            ]);
    }

    /** @test */
    public function login_with_no_email()
    {
        $this->json('POST', 'login', ['email' => ''])
            ->seeJson([
                "email" => ["The email field is required."]
            ]);
    }
}
