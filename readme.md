# Staff Site
A social network application for restaurants workers I wrote as a hobby while I studied and worked. 
It explored the idea of using a package manager and Browserify to compile CSS, Javascript and other assets.

# Instructions

Simply follow the good old procedures required from any Laravel Project:

`composer install`
`npm install`
`php artisan migrate`
`php artisan db:seed`

# Tests

`php unit`